﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SimpleLang.MiddleEnd;

namespace SimpleLang.Analysis
{
    using Edge = SpanningTree.Edge;
    using Node = DominatorsTree.TreeNode<BaseBlock>;
    public class EdgeClassification
    {
        // Обратные, т.е. конечная доминирует над начальной    
        private List<Edge> backwardEdges; // = new List<Edge>(); 
        // Наступающий - указывает на потомка, который будет пройден на след шагу
        private List<Edge> forwardEdges;
        // Отступающий - указывает на предка, который уже был пройден
        private List<Edge> retreatEdges;
        // Поперечный - указывает на элемент, ни один не является предком другого
        private List<Edge> transverseEdges;

        private List<BaseBlock> stackHistory;
        private ControlFlowGraph cfg;
        private DominatorsTree dt;

        public EdgeClassification(ControlFlowGraph cfg)
        {
            this.backwardEdges = new List<Edge>();
            this.forwardEdges = new List<Edge>();
            this.retreatEdges = new List<Edge>();
            this.transverseEdges = new List<Edge>();

            this.stackHistory = new List<BaseBlock>();
            DominatorsTree dt = new DominatorsTree(cfg);

            this.cfg = cfg;
            this.dt = dt;
            foreach (BaseBlock bb in cfg.GetBlocks().Where(bl => !this.stackHistory.Contains(bl) && bl != this.cfg.GetStart() && bl != this.cfg.GetEnd()))
            {
                Visit(bb);
            }
       }

        public List<Edge> forward { get { return forwardEdges; } }
        public List<Edge> backward { get { return backwardEdges; } }
        public List<Edge> retreat { get { return retreatEdges; } }
        public List<Edge> transverse { get { return transverseEdges; } }

        // Получает доминатора бб
        public BaseBlock getDominator(BaseBlock bb, Node start)
        {
            
            Stack<DominatorsTree.TreeNode<BaseBlock>> Path =
                new Stack<DominatorsTree.TreeNode<BaseBlock>>();
            Path.Push(this.dt.Root);
            while (Path.Count > 0)
            {
                var Top = Path.Pop();
                if (Top.Items.Count > 0)
                {
                    foreach (var it in Top.Items)
                    {
                        if (it.Value.nBlock == bb.nBlock)
                            return Top.Value;
                        Path.Push(it);
                    }
                }
            }
            return new BaseBlock();
        }

        // Проверяет доминирует ли src над dst
        public bool dominates(BaseBlock src, BaseBlock dst, Node start)
        {
            Stack<DominatorsTree.TreeNode<BaseBlock>> Path =
                new Stack<DominatorsTree.TreeNode<BaseBlock>>();
            Path.Push(this.dt.Root);
            while (Path.Count > 0)
            {
                var Top = Path.Pop();
                if (Top.Items.Count > 0)
                {
                    foreach (var it in Top.Items)
                    {
                        if (Top.Value == src && it.Value == dst)
                            return true;
                        Path.Push(it);
                    }
                }                
            }
            return false;
        }

        // Проверяет на поперечность
        public bool checkTransverse(BaseBlock src, BaseBlock dst, Node start)
        {
              return (src == getDominator(dst, this.dt.Root) || dst == getDominator(src, this.dt.Root));
        }

        public void Visit(BaseBlock bb)
        {
            // Мы рассматриваем, значит сразу добавляем
            stackHistory.Add(bb);
            foreach (BaseBlock child in cfg.GetOutputs(bb).Where(bl => bl != this.cfg.GetStart() && bl != this.cfg.GetEnd()))
            {
                // Поиск в глубину, если не встречали этого наследника, то это прямой
                if (!stackHistory.Contains(child))
                {
                    forwardEdges.Add(new Edge(bb.nBlock, child.nBlock));
                    // Добавляем, рекурсиовно посещаем
                    Visit(child);
                }
                else
                {
                    // Иначе это может быть поперечный
                    if (!checkTransverse(bb, child, this.dt.Root))
                    {
                        transverseEdges.Add(new Edge(bb.nBlock, child.nBlock));
                    }
                    else
                    {
                        // Иначе это точно отступающая
                        retreatEdges.Add(new Edge(bb.nBlock, child.nBlock));
                        // А может еще и обратная
                        if (dominates(child, bb, this.dt.Root))
                        {
                            backwardEdges.Add(new Edge(bb.nBlock, child.nBlock));
                        }
                    }
                }
            }
        }

        public static void printEdgeList(List<Edge> edges)
        {
            if (edges == null)
                return;
            foreach (Edge edge in edges)
                Console.WriteLine(edge);
        }

        public void Print()
        {
            Console.WriteLine("Наступающие ребра");
            printEdgeList(forwardEdges);
            Console.WriteLine("Отступающие ребра");
            printEdgeList(retreatEdges);
            Console.WriteLine("Обратные ребра");
            printEdgeList(backwardEdges);
            Console.WriteLine("Поперечные ребра");
            printEdgeList(transverseEdges);
        }
    }
}
