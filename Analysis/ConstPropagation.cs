﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleLang.MiddleEnd;

namespace SimpleLang.Analysis
{
    public class ConstPropTable : Dictionary<String, String>, IEquatable<ConstPropTable>
    {
        public ConstPropTable() : base() { foreach (var v in SymbolTable.vars) if (v.Value.Item2 == SymbolKind.var) this.Add(v.Key, "UNDEF"); }
        public ConstPropTable(Object obj) : base() { foreach (var v in SymbolTable.vars) if (v.Value.Item2 == SymbolKind.var) this.Add(v.Key, null); }
        public bool Equals(ConstPropTable other)
        {
            if (this.Count != other.Count) return false;
            foreach (var a in this)
            {
                if (other[a.Key] != a.Value) return false;
            }
            return true;
        }
        public void Print()
        {
            foreach (var a in this)
            {
                Console.WriteLine(a.Key + " : " + a.Value);
            }
        }
        public static ConstPropTable Union(ConstPropTable a, ConstPropTable b)
        {
            ConstPropTable r = new ConstPropTable(null);
            foreach (var l in a)
            {
                if (a[l.Key] != b[l.Key]) r[l.Key] = "UNDEF";
                else if (a[l.Key] != b[l.Key]) r[l.Key] = a[l.Key];
            }
            return r;
        }
    }
    public class ConstPropContext : Context<ConstPropTable>
    {
        public ConstPropContext(ControlFlowGraph cfg)
            : base(cfg)
        {
            foreach (BaseBlock block in cfg.GetBlocks())
            {
                this[block] = new ConstPropTable(null);
                foreach (CodeLine line in block.Code)
                {
                    if (line.Operator == OperatorType.Assign)
                    {

                        switch (line.BinOp)
                        {
                            case BinOpType.None:
                                if (this[block].ContainsKey(line.Second) && this[block][line.Second] != null)
                                    this[block][line.First] = this[block][line.Second];
                                else
                                    this[block][line.First] = line.Second;
                                break;
                            case BinOpType.Plus:
                                if (this[block].ContainsKey(line.Second) && this[block][line.Second] != null)
                                    this[block][line.First] = this[block][line.Second];
                                else
                                    this[block][line.First] = line.Second;
                                this[block][line.First] += "|+|";
                                if (this[block].ContainsKey(line.Third) && this[block][line.Third] != null)
                                    this[block][line.First] += this[block][line.Third];
                                else
                                    this[block][line.First] += line.Third;
                                break;
                            case BinOpType.Minus:
                                if (this[block].ContainsKey(line.Second) && this[block][line.Second] != null)
                                    this[block][line.First] = this[block][line.Second];
                                else
                                    this[block][line.First] = line.Second;
                                this[block][line.First] += "|-|";
                                if (this[block].ContainsKey(line.Third) && this[block][line.Third] != null)
                                    this[block][line.First] += this[block][line.Third];
                                else
                                    this[block][line.First] += line.Third;
                                break;
                            case BinOpType.Mult:
                                if (this[block].ContainsKey(line.Second) && this[block][line.Second] != null)
                                    this[block][line.First] = this[block][line.Second];
                                else
                                    this[block][line.First] = line.Second;
                                this[block][line.First] += "|*|";
                                if (this[block].ContainsKey(line.Third) && this[block][line.Third] != null)
                                    this[block][line.First] += this[block][line.Third];
                                else
                                    this[block][line.First] += line.Third;
                                break;
                            case BinOpType.Div:
                                if (this[block].ContainsKey(line.Second) && this[block][line.Second] != null)
                                    this[block][line.First] = this[block][line.Second];
                                else
                                    this[block][line.First] = line.Second;
                                this[block][line.First] += "|/|";
                                if (this[block].ContainsKey(line.Third) && this[block][line.Third] != null)
                                    this[block][line.First] += this[block][line.Third];
                                else
                                    this[block][line.First] += line.Third;
                                break;
                            default: this[block][line.First] = "NAC"; break;
                        }
                    }
                }
            }
        }
        public void Print()
        {
            foreach (var b in this.Blocks)
            {
                Console.WriteLine("Блок ==========");
                foreach (var a in this[b]) Console.WriteLine(a.Key + " : " + a.Value);
            }
        }
        public void Print(int i)
        {
            foreach (var a in this[this.Blocks.ElementAt(i)]) Console.WriteLine(a.Key + " : " + a.Value);
        }
    }

    public class ConstPropAlgorithm : TopDownAlgorithm<ConstPropTable, ConstPropContext, ConstPropTable>
    {
        public ConstPropAlgorithm(ControlFlowGraph cfg)
            : base(cfg)
        {
            foreach (BaseBlock bl in cfg.GetBlocks())
            {
                In[bl] = new ConstPropTable();
                Out[bl] = new ConstPropTable();
                Func[bl] = new ConstPropTransferFunction(Cont[bl]);
            }
        }

        public override Tuple<Dictionary<BaseBlock, ConstPropTable>, Dictionary<BaseBlock, ConstPropTable>> Apply()
        {
            return base.Apply(new ConstPropTable(), new ConstPropTable(), new ConstPropTable(), ConstPropTable.Union);
        }
    }

    public class ConstPropTransferFunction : InfoProvidedTransferFunction<ConstPropTable, ConstPropTable>
    {
        public ConstPropTransferFunction(ConstPropTable info) : base(info) { }

        public override ConstPropTable Transfer(ConstPropTable min)
        {
            var mout = new ConstPropTable();
            foreach (var m in min)
            {
                if (Info[m.Key] != null)
                {
                    int n, n1;
                    if (Info[m.Key].Equals("NAC") || Info[m.Key].Equals("UNDEF") || (Int32.TryParse(Info[m.Key], out n) && n.ToString().Equals(Info[m.Key])))
                    {
                        mout[m.Key] = Info[m.Key];
                        continue;
                    }
                    string[] str = Info[m.Key].Split(new Char[] { '|' });
                    if (str.Length == 3)
                    {
                        if (str.Contains<string>("NAC")) mout[m.Key] = "NAC";
                        else if (Int32.TryParse(str[0], out n) && n.ToString().Equals(str[0]) && Int32.TryParse(str[2], out n1) && n1.ToString().Equals(str[2]))
                        {
                            switch (str[1])
                            {
                                case "+": mout[m.Key] = (n + n1).ToString(); break;
                                case "-": mout[m.Key] = (n - n1).ToString(); break;
                                case "*": mout[m.Key] = (n * n1).ToString(); break;
                                case "/": mout[m.Key] = (n / n1).ToString(); break;
                                default: mout[m.Key] = "UNDEF"; break;
                            }
                        }
                        else mout[m.Key] = "UNDEF";
                    }
                }
            }
            return mout;
        }
    }
}
