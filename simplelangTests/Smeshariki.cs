﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleCompiler;
using SimpleLang.Analysis;
using SimpleLang.MiddleEnd;
using SimpleLang.Optimizations;
using System;
using System.Collections.Generic;

namespace simplelangTests
{
    [TestClass]
    public class Smeshariki
    {
        [TestMethod]
        public void CSE_04()
        {
            BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/CSE_04.txt");
            if (Root != null && SimpleCompilerMain.SemanticAnalysis(Root))
            {
                var CFG = SimpleCompilerMain.BuildCFG(Root);
                CSE cse = new CSE();
                SimpleCompilerMain.PrintCFG(CFG);
                cse.Optimize(CFG.GetBlocks().First.Next.Value);
                SimpleCompilerMain.PrintCFG(CFG);
                string f = CFG.GetBlocks().First.Next.Value.Code.First.Value.First;
                Assert.IsTrue(f.StartsWith("_t"));
                Assert.IsTrue(CFG.GetBlocks().First.Next.Value.Code.First.Value.Second.Equals("b"));
                Assert.IsTrue(CFG.GetBlocks().First.Next.Value.Code.First.Value.Third.Equals("c"));
            }
        }

        [TestMethod]
        public void CSE_04_2()
        {
            BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/CSE_04_2.txt");
            if (Root != null && SimpleCompilerMain.SemanticAnalysis(Root))
            {
                var CFG = SimpleCompilerMain.BuildCFG(Root);
                SimpleCompilerMain.PrintCFG(CFG);
                CSE cse = new CSE();
                foreach (BaseBlock block in CFG.GetBlocks())
                {
                    if (block != CFG.GetStart() && block != CFG.GetEnd())
                    {
                        cse.Optimize(block);
                    }
                }
                SimpleCompilerMain.PrintCFG(CFG);
                List<CodeLine> cl = new List<CodeLine>(CFG.BlockAt(3).Code);
                Assert.IsFalse(cl[0].Label == "_l0" && cl[1].Label == "_l0");
            }
        }

        [TestMethod]
        public void SpanningTree_26()
        {
            BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/Test3.txt");
            if (Root != null && SimpleCompilerMain.SemanticAnalysis(Root))
            {
                var CFG = SimpleCompilerMain.BuildCFG(Root);
                var l = CFG.GetListBlocks();
                foreach (var item in l)
                {
                    Console.WriteLine(item.nBlock + " " + l.IndexOf(item));
                }
                SpanningTree spTree = new SpanningTree(CFG);
                foreach (var item in l)
                {
                    Console.WriteLine(item.nBlock + " " + l.IndexOf(item));
                }
                Assert.IsTrue(l[7].nBlock == 6 && l[8].nBlock == 3);//проверяем перенумеровали или нет
            }
        }
        [TestMethod]
        public void SpanningTree_26_2()
        {
            BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/SpanningTree_26_2.txt");
            if (Root != null && SimpleCompilerMain.SemanticAnalysis(Root))
            {
                var CFG = SimpleCompilerMain.BuildCFG(Root);
                AliveVarsAlgorithm AVA = new AliveVarsAlgorithm(CFG);
                var AVAResult = AVA.Apply();
                int oldcounter = AliveVarsAlgorithm.IterCounter;
                Console.WriteLine(oldcounter);
                SpanningTree spTree = new SpanningTree(CFG);
                AliveVarsAlgorithm AVA2 = new AliveVarsAlgorithm(CFG);
                var AVAResult2 = AVA.Apply();
                int newcounter = AliveVarsAlgorithm.IterCounter;
                Console.WriteLine(newcounter);
                Assert.IsTrue(newcounter<oldcounter);
            }
        }
        [TestMethod]
        public void ConstProp_16()
        {
            BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/ConstProp.txt");
            if (Root != null && SimpleCompilerMain.SemanticAnalysis(Root))
            {
                var CFG = SimpleCompilerMain.BuildCFG(Root);
                SimpleCompilerMain.PrintCFG(CFG);
                Console.WriteLine();
                Console.WriteLine("Анализ распространения констант.");
                ConstPropContext context = new ConstPropContext(CFG);
                Console.WriteLine("Передаточная функция ББл");
                context.Print(1);
                ConstPropAlgorithm alg = new ConstPropAlgorithm(CFG);
                Tuple<Dictionary<BaseBlock, ConstPropTable>, Dictionary<BaseBlock, ConstPropTable>> ads = alg.Apply();
                Console.WriteLine("Результат применения передаточной функции");
                ads.Item2[CFG.BlockAt(1)].Print();
                Assert.AreEqual("UNDEF", ads.Item2[CFG.BlockAt(1)]["i1"]);
                Assert.AreEqual("UNDEF", ads.Item2[CFG.BlockAt(1)]["i2"]);
            }
        }

        [TestMethod]
        public void CSE_04_3()
        {
            LinkedList<CodeLine> blCode;
            try
            {
                BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/CSE_04.txt");
                SimpleCompilerMain.SemanticAnalysis(Root);
                var cfg = SimpleCompilerMain.BuildCFG(Root);
                BaseBlock block1 = cfg.BlockAt(1);
                (new CSE()).Optimize(block1);
                //SimpleCompilerMain.PrintCommands(block1.Code);
                blCode = block1.Code;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            LinkedListNode<CodeLine> firstCmd = null;
            try
            {
                firstCmd = blCode.First;
            }
            catch (NullReferenceException ne)
            {
                Console.WriteLine("blCode: " + ne.Message);
                Assert.Fail("Объект для тестирования не инициализирован");
            }
            Assert.IsTrue(firstCmd.Value.First.StartsWith("_to"), "для выражения b+c не сохранен результат вычисления");
            Assert.IsTrue(firstCmd.Next.Value.Second.Equals(firstCmd.Value.First), "неверное использование временной переменной " + firstCmd.Value.First);
            Assert.IsTrue(firstCmd.Next.Next.Value.Second.Equals(firstCmd.Value.First), "неверное использование временной переменной " + firstCmd.Value.First);
        }

        [TestMethod]
        public void CSE_04_4()
        {
            LinkedList<CodeLine> blCode;
            try
            {
                BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/CSE_04.txt");
                SimpleCompilerMain.SemanticAnalysis(Root);
                var cfg = SimpleCompilerMain.BuildCFG(Root);
                BaseBlock block1 = cfg.BlockAt(1);
                (new CSE()).Optimize(block1);
                //SimpleCompilerMain.PrintCommands(block1.Code);
                blCode = block1.Code;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            List<CodeLine> lblocks = null;
            try
            {
                lblocks = new List<CodeLine>(blCode);
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("blCode: " + ane.Message);
                Assert.Fail("Объект для тестирования не инициализирован");
            }
            string tName = lblocks[5].First;
            Assert.IsTrue(tName.StartsWith("_to"), "для выражения a-d не сохранен результат вычисления");
            Assert.IsTrue(lblocks[6].Second.Equals(tName), "неверное использование временной переменной " + tName);
            Assert.IsTrue(lblocks[8].Second.Equals(tName), "неверное использование временной переменной " + tName);
        }

        [TestMethod]
        public void CSE_04_5()
        {
            LinkedList<CodeLine> blCode;
            try
            {
                BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/CSE_04.txt");
                SimpleCompilerMain.SemanticAnalysis(Root);
                var cfg = SimpleCompilerMain.BuildCFG(Root);
                BaseBlock block1 = cfg.BlockAt(1);
                (new CSE()).Optimize(block1);
                //SimpleCompilerMain.PrintCommands(block1.Code);
                blCode = block1.Code;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            List<CodeLine> lblocks = null;
            try
            {
                lblocks = new List<CodeLine>(blCode);
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("blCode: " + ane.Message);
                Assert.Fail("Объект для тестирования не инициализирован");
            }
            Assert.IsFalse(lblocks[7].Second.StartsWith("_to"), "неверный результат оптимизации выражения b+c");
        }

        [TestMethod]
        public void CSE_04_6()
        {
            LinkedList<CodeLine> blCode;
            try
            {
                BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/CSE_04.txt");
                SimpleCompilerMain.SemanticAnalysis(Root);
                var cfg = SimpleCompilerMain.BuildCFG(Root);
                BaseBlock block1 = cfg.BlockAt(1);
                (new CSE()).Optimize(block1);
                //SimpleCompilerMain.PrintCommands(block1.Code);
                blCode = block1.Code;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            List<CodeLine> lblocks = null;
            try
            {
                lblocks = new List<CodeLine>(blCode);
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("blCode: " + ane.Message);
                Assert.Fail("Объект для тестирования не инициализирован");
            }
            Assert.IsFalse(lblocks[9].First.StartsWith("_to"), "неверный результат оптимизации выражения d+1");
        }
    }
}