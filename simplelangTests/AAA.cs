﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleCompiler;
using SimpleLang.Analysis;
using SimpleLang.MiddleEnd;
using SimpleLang.Optimizations;
using SimpleParser;
using SimpleScanner;
using System;
using System.Collections.Generic;
using System.IO;

namespace simplelangTests
{
    [TestClass]
    public class AAA
    {
        [TestMethod]
        public void CleanDead_06()
        {
            BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/CleanDead_06.txt");
            if (Root != null && SimpleCompilerMain.SemanticAnalysis(Root))
            {
                var CFG = SimpleCompilerMain.BuildCFG(Root);
                Console.WriteLine("До оптимизации:");
                Console.WriteLine(CFG.BlockAt(1));
                CleanDead cl = new CleanDead();
                cl.Optimize(CFG.BlockAt(1));
                Console.WriteLine();
                Console.WriteLine("После оптимизации:");
                Console.WriteLine(CFG.BlockAt(1));
                Assert.AreEqual(4, CFG.BlockAt(1).Code.Count);
            }
        }
        [TestMethod]
        public void CleanDead_06_2()
        {
            BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/CleanDead_06_2.txt");
            if (Root != null && SimpleCompilerMain.SemanticAnalysis(Root))
            {
                var CFG = SimpleCompilerMain.BuildCFG(Root);
                CSE cse = new CSE();
                SimpleCompilerMain.PrintCFG(CFG);
                cse.Optimize(CFG.GetBlocks().First.Next.Value);
                SimpleCompilerMain.PrintCFG(CFG);
                CleanDead cl = new CleanDead();
                while (cl.Optimize(CFG.GetBlocks().First.Next.Value)){}                
                SimpleCompilerMain.PrintCFG(CFG);
                Assert.AreEqual(3, CFG.GetBlocks().First.Next.Value.Code.Count);
            }
        }
        [TestMethod]
        public void CleanDead_06_3()
        {
            BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/CleanDead_06_3.txt");
            if (Root != null && SimpleCompilerMain.SemanticAnalysis(Root))
            {
                var CFG = SimpleCompilerMain.BuildCFG(Root);
                
                SimpleCompilerMain.PrintCFG(CFG);
                CleanDead cl = new CleanDead();
                foreach (BaseBlock block in CFG.GetBlocks())
                {
                    if (block != CFG.GetStart() && block != CFG.GetEnd())
                    {
                        while (cl.Optimize(block)) { }
                    }
                }
                SimpleCompilerMain.PrintCFG(CFG);
                Assert.AreEqual("_l0", CFG.BlockAt(3).Code.First.Value.Label);
            }
        }
        [TestMethod]
        public void FormulaTF_10()
        {
             BlockNode Root = SimpleCompilerMain.SyntaxAnalysis("../../_Texts/Test2.txt");
             if (Root != null && SimpleCompilerMain.SemanticAnalysis(Root))
             {
                 var CFG = SimpleCompilerMain.BuildCFG(Root);

                 KillGenContext cont = new KillGenContext(CFG);
                 List<Tuple<BitSet, BitSet>> tupl = new List<Tuple<BitSet, BitSet>>();
                 foreach (var line in CFG.GetBlocks().First.List)
                 {
                     tupl.Add(new Tuple<BitSet, BitSet>(cont.Gen(line), cont.Kill(line)));
                 }
                 FormulaTransferFunction tf = new FormulaTransferFunction(tupl);
                 BitSet input = new BitSet(cont.Count);
                 BitSet output = tf.Transfer(input);
                 BitSet success = new BitSet(new bool[] { true, false, true, false, false });

                 Assert.IsTrue(output.Equals(success));
             }
        }
    }
}