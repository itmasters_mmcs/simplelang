gitinspector -wHL -f cs,lex,y,txt -F htmlembedded > report.html
gitinspector -wHL -f cs,lex,y -F htmlembedded > report_no_txt.html
gitinspector -wHL -f cs -F htmlembedded > report_nolex_noyac.html
echo "stat by changes"
echo
git log --format='%aN' | sort -u | while read name; do echo -en "$name\t"; git log --author="$name" --pretty=tformat: --numstat | awk '{ add += $1; subs += $2; loc += $1 - $2 } END { printf "added lines: %s, removed lines: %s, total lines: %s\n", add, subs, loc }' -; done
echo "stat by commits"
echo
echo
git shortlog -sn